﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;
using SNA.common;
using SNA.DataAccess;
using SNA.IoCConfig;
using SNA.Proccess.Proccess;
using SNA.Services.Store;

namespace SNA.Proccess
{
	public class Startup
	{
		public static IServiceProvider ServiceProvider { get; set; }
		public IConfigurationRoot Configuration { get; }
		public int IOptionsSnapShot { get; private set; }
		private Microsoft.AspNetCore.Hosting.IHostingEnvironment _currentEnvironment;
		private IHttpContextAccessor _contextAccessor;

		public Startup(Microsoft.AspNetCore.Hosting.IHostingEnvironment env, IHttpContextAccessor contextAccessor)
		{
			var builder = new ConfigurationBuilder()
				.SetBasePath(env.ContentRootPath)
				.AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
				.AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
				.AddEnvironmentVariables();
			Configuration = builder.Build();

			_currentEnvironment = env;
			_contextAccessor = contextAccessor;
		}

		// This method gets called by the runtime. Use this method to add services to the container.
		public void ConfigureServices(IServiceCollection services)
		{
			services.AddSingleton<Microsoft.AspNetCore.Http.IHttpContextAccessor, HttpContextAccessor>();

			services.Configure<AppConfig>(options => Configuration.GetSection("AppConfig").Bind(options));

			services.AddSingleton(p => ServiceProvider);

			services.AddDbContext<SNADbContext>(options =>
			{
				options.UseSqlServer(
					ServiceProvider.GetService<IOptionsSnapshot<AppConfig>>().Value.SQLConnectionString,
					b => b.MigrationsAssembly("SNA.DataAccess")
				);
				options.EnableSensitiveDataLogging(true);
			});

			services.AddAutoMapper();
			services.AddTransiantDataServices();
			services.AddTransientRepositoryServices();
			services.AddTransientSNAServices();

			ServiceProvider = services.BuildServiceProvider();

			InstaUserModelStore.Init();

			//services.AddSingleton<IHostedService, InstaMediaCrawler>();
			//services.AddSingleton<IHostedService, InstaMediaCommentsCrawler>();
			//services.AddSingleton<IHostedService, InstaMediaLikerCrawler>();
			//services.AddSingleton<IHostedService, InstaFollowersCrawler>();
			services.AddSingleton<IHostedService, InstaUserStoreFromDBCrawler>();
			services.AddSingleton<IHostedService, InstaUserStoreToDBCrawler>();
			services.AddSingleton<IHostedService, InstaUserInfoCrawler>();
            services.AddSingleton<IHostedService, InstaUserDbStoreFromDB>();
		}

		public void Configure(IApplicationBuilder app, Microsoft.AspNetCore.Hosting.IHostingEnvironment env)
		{
			if (env.IsDevelopment())
			{
				app.UseDeveloperExceptionPage();
			}
		}
	}
}
