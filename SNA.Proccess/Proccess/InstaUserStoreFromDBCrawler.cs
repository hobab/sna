﻿using SNA.Instagram;
using SNA.Services;
using SNA.Services.Store;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace SNA.Proccess.Proccess
{
	public class InstaUserStoreFromDBCrawler : BackgroundService
	{
		private IInstaUserService _service;

		public InstaUserStoreFromDBCrawler(IInstaUserService service) : base()
		{
			_service = service;
		}
		protected override Task ExecuteAsync(CancellationToken stoppingToken)
		{
			Task.Run(async () =>
			{
				while (true)
				{
					await _service.InstaUserStoreFromDB();
				}
			});
			return Task.FromResult(0);
		}
	}
}
