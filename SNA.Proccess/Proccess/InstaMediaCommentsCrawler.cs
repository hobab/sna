﻿using SNA.Instagram;
using SNA.Services;
using SNA.Services.Store;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace SNA.Proccess.Proccess
{
	public class InstaMediaCommentsCrawler : BackgroundService
	{
		private IInstaMediaService _service;

		public InstaMediaCommentsCrawler(IInstaMediaService service) : base()
		{
			_service = service;
		}
		protected override Task ExecuteAsync(CancellationToken stoppingToken)
		{
			Task.Run(async () =>
			{
				while (true)
				{
					await _service.CrawlMediaCommentsAsync();
				}
			});
			return Task.FromResult(0);
		}
	}
}
