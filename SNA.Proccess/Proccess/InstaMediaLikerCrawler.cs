﻿using SNA.Instagram;
using SNA.Services;
using SNA.Services.Store;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace SNA.Proccess.Proccess
{
	public class InstaMediaLikerCrawler : BackgroundService
	{
		private IInstaMediaService _service;

		public InstaMediaLikerCrawler(IInstaMediaService service) : base()
		{
			_service = service;
		}
		protected override Task ExecuteAsync(CancellationToken stoppingToken)
		{
			Task.Run(async () =>
			{
				while (true)
				{
					await _service.CrawlMediaLikersAsync();
				}
			});
			return Task.FromResult(0);
		}
	}
}
