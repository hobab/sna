﻿using SNA.Instagram;
using SNA.Services;
using SNA.Services.Store;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace SNA.Proccess.Proccess
{
	public class InstaUserDbStoreFromDB : BackgroundService
	{
		private IInstaUserService _service;

		public InstaUserDbStoreFromDB(IInstaUserService service) : base()
		{
			_service = service;
		}
		protected override Task ExecuteAsync(CancellationToken stoppingToken)
		{
			try
			{
				Task.Run(async () =>
				{
					while (true)
					{
						await _service.InstaUserDbStoreFromDB();
					}
				});
			}catch(Exception ex)
			{

			}
			return Task.FromResult(0);
		}
	}
}
