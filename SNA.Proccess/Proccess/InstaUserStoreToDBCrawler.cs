﻿using SNA.Instagram;
using SNA.Services;
using SNA.Services.Store;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace SNA.Proccess.Proccess
{
	public class InstaUserStoreToDBCrawler : BackgroundService
	{
		private IInstaUserService _service;

		public InstaUserStoreToDBCrawler(IInstaUserService service) : base()
		{
			_service = service;
		}
		protected override Task ExecuteAsync(CancellationToken stoppingToken)
		{
			try
			{
				Task.Run(async () =>
				{
					while (true)
					{
						await _service.InstaUserStoreToDB();
					}
				});
			}catch(Exception ex)
			{

			}
			return Task.FromResult(0);
		}
	}
}
