﻿using InstagramApiSharp;
using SNA.Instagram;
using System;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace SNA.Test
{
	class Program
	{
		static async Task Main(string[] args)
		{
			try
			{
				var crawler = new CrawlerInstagram(new AuthInstagram());
				await crawler.Login("hamid.reza.526", "P@ssw0rd++");
				var paggination = PaginationParameters.MaxPagesToLoad(1);
				var ss = await crawler.GetUserMedias("mah_food", paggination);
				//var dd = await crawler.GetMediaComments("1000042963669964159", paggination); 
				foreach (var com in ss.Value)
				{
					var regex = new Regex(@"(?<=#)\w+");
					var matches = regex.Matches(com.Caption.Text);

					foreach (Match m in matches)
					{
						Console.WriteLine(m.Value);
					}
				}
			}
			catch (Exception ex)
			{

			}
		}
	}
}
