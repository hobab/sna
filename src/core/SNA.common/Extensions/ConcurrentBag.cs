﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Text;

namespace SNA.common.Extensions
{
	public static class ConcurrentBagExtentions
	{
		public static void AddRange<T>(this ConcurrentBag<T> @this, IEnumerable<T> toAdd)
		{
			foreach (var element in toAdd)
			{
				@this.Add(element);
			}
		}
	}
}
