﻿namespace SNA.Entities
{
	public class InstaUserFollow
	{
		public long SourcePK { get; set; }
		public long TargetPK { get; set; }

		public InstaUser Source { get; set; }
		public InstaUser Target { get; set; }
	}
}
