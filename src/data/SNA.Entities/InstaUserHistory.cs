﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SNA.Entities
{
    public class InstaUserHistory
    {
		public long UserPK { get; set; }
		public DateTime HistoryDate { get; set; }
		public long FollowerCount { get; set; }
		public long FollowingCount { get; set; }
		public long FollowingTagCount { get; set; }
		public long MediaCount { get; set; }
		public long UsertagsCount { get; set; }

		public InstaUser InstaUser { get; set; }
	}
}
