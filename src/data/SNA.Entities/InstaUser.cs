﻿using System.Collections.Generic;

namespace SNA.Entities
{
	public class InstaUser : BaseEntity
	{
		public InstaUser()
		{
			Followers = new HashSet<InstaUserFollow>();
			Followings = new HashSet<InstaUserFollow>();
			Medias = new HashSet<InstaMedia>();
			MediaLikers = new HashSet<InstaMediaLiker>();
			MediaComments = new HashSet<InstaMediaComment>();
			UserHistories = new HashSet<InstaUserHistory>();
		}

		public long PK { get; set; }
		public string FullName { get; set; }
		public bool IsPrivate { get; set; }
		public bool IsVerified { get; set; }
		public string ProfilePicUrl { get; set; }
		public string Username { get; set; }

		public string AddressStreet { get; set; }
		public string Biography { get; set; }
		public string Category { get; set; }
		public long CityId { get; set; }
		public string CityName { get; set; }
		public string ContactPhoneNumber { get; set; }
		public string DirectMessaging { get; set; }
		public string ExternalLynxUrl { get; set; }
		public string ExternalUrl { get; set; }
		public long FollowerCount { get; set; }
		public long FollowingCount { get; set; }
		public long FollowingTagCount { get; set; }
		public string FriendshipStatus { get; set; }
		public long GeoMediaCount { get; set; }
		public long MediaCount { get; set; }
		public string PageName { get; set; }
		public string PublicEmail { get; set; }
		public string PublicPhoneNumber { get; set; }
		public long UsertagsCount { get; set; }

		public ICollection<InstaUserFollow> Followers { get; set; }
		public ICollection<InstaUserFollow> Followings { get; set; }
		public ICollection<InstaMedia> Medias { get; set; }
		public ICollection<InstaMediaLiker> MediaLikers { get; set; }
		public ICollection<InstaMediaComment> MediaComments { get; set; }
		public ICollection<InstaUserHistory> UserHistories { get; set; }
	}
}
