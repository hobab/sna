﻿using System;

namespace SNA.Entities
{
	public	class BaseEntity
	{
		public DateTime? CrawledDate { get; set; }
		public DateTime? NextCrawlDate { get; set; }
	}
}
