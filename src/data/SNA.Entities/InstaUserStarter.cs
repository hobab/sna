﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SNA.Entities
{
    public class InstaUserStarter:BaseEntity
    {
		public string Username { get; set; }
		public long? UserPK { get; set; }
		public DateTime? MediaCrawledDate { get; set; }
		public DateTime? MediaNextCrawlDate { get; set; }
	}
}
