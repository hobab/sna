﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SNA.Entities
{
    public class InstaMediaLiker
    {
		public string MediaPK { get; set; }
		public long UserPK { get; set; }

		public InstaMedia Media { get; set; }
		public InstaUser User { get; set; }
    }
}
