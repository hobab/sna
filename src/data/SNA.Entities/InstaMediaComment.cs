﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SNA.Entities
{
    public class InstaMediaComment
    {
		public long PK { get; set; }
		public string MediaPK { get; set; }
		public long UserPK { get; set; }
		public string Text { get; set; }
		public DateTime CreatedAt { get; set; }
		public int LikesCount { get; set; }


		public	InstaMedia Media { get; set; }
		public InstaUser User { get; set; }
    }
}
