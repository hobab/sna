﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace SNA.Entities
{
	public class InstaMedia:BaseEntity
	{
		public InstaMedia()
		{
			MediaLikers = new HashSet<InstaMediaLiker>();
			MediaComments = new HashSet<InstaMediaComment>();
		}

		public string PK { get; set; }
		public long UserPK { get; set; }
		public int? Width { get; set; }
		public int? Height { get; set; }
		public int LikesCount { get; set; }
		public int CommentsCount { get; set; }
		public string NextMediaId { get; set; }
		public string Title { get; set; }
		public DateTime TakenAt { get; set; }
		public string Caption { get; set; }
		public bool CaptionIsEdited { get; set; }
		public string ImageUrl { get; set; }
		public DateTime? LikerNextCrawlDate { get; set; }
		public DateTime? CommentersNextCrawlDate { get; set; }
		public int FollowLikeCount { get; set; }

		public InstaUser User { get; set; }
		public ICollection<InstaMediaLiker> MediaLikers { get; set; }
		public ICollection<InstaMediaComment> MediaComments { get; set; }
	}
}
