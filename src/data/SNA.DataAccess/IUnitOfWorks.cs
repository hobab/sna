﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace SNA.DataAccess
{
    public interface IUnitOfWork
    {
        IRepo<T> Repository<T>() where T : class;
        Task CommitAsync(Func<Task> func);
        Task ExecSqlCommand(string command);
    }

    public interface IRepo<T> where T : class
    {
        IQueryable<T> AsQueryable();
        void Delete(IEnumerable<T> entities);
        void Delete(T entity);
        Task DeleteAsync(IEnumerable<T> entities);
        Task DeleteAsync(T entity);
        void Insert(IEnumerable<T> entities);
        Task BulkInsertAsync(IList<T> entities);
        Task BulkInsertSubEntityAsync(IList<T> entities);
        void Insert(T entity);
        Task InsertAsync(IEnumerable<T> entities);
        Task InsertAsync(T entity);
        void Update(IEnumerable<T> entities);
        void Update(T entity);
        Task UpdateAsync(IEnumerable<T> entities);
        Task UpdateAsync(T entity);
        Task BulkUpdateAsync(IList<T> entities);
        Task<int> SaveChangesAsync();
    }

    public interface IDbContext
    {
        void Migrate();
        void Seed();
        DbSet<TEntity> Set<TEntity>() where TEntity : class;
        EntityEntry<TEntity> Entry<TEntity>(TEntity entity) where TEntity : class;
        int SaveChanges();
        Task<int> SaveChangesAsync(CancellationToken ct = default(CancellationToken));
    }
}
