﻿using System;
using System.Collections;
using System.IO;
using System.Threading.Tasks;
using System.Transactions;
using Microsoft.EntityFrameworkCore;

namespace SNA.DataAccess
{
    public class SNADbContext : DbContext, IDbContext, IUnitOfWork
    {
        public SNADbContext(DbContextOptions options) : base(options)
        {

        }

        #region uow
        #region Fields
        private Hashtable _repositories;
        private bool _disposed;

        public IRepo<TEntity> Repository<TEntity>() where TEntity : class
        {
            if (_repositories == null)
            {
                _repositories = new Hashtable();
            }

            var type = typeof(TEntity).Name;

            if (_repositories.ContainsKey(type))
            {
                return (IRepo<TEntity>)_repositories[type];
            }

            var repositoryType = typeof(Repo<>);

            try
            {
                _repositories.Add(type, Activator.CreateInstance(repositoryType.MakeGenericType(typeof(TEntity)), this));

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return (IRepo<TEntity>)_repositories[type];
        }

        #endregion
        public async Task CommitAsync(Func<Task> func)
        {
            var strategy = this.Database.CreateExecutionStrategy();

                await strategy.ExecuteAsync(async()=>
                {
					//using (var transaction = await this.Database.BeginTransactionAsync())
					//{
					//    try
					//    {
					//        await func.Invoke();
					//        transaction.Commit();
					//    }

					//    catch(Exception ex)
					//    {
					//        transaction.Rollback();
					//        throw ex;
					//    }
					//}
					using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
					{
						try
						{
							await func.Invoke();
							scope.Complete();
						}
						catch (Exception ex)
						{
							throw ex;
						}
					}
                });
        }

        public async Task ExecSqlCommand(string command)
        {
            this.Database.SetCommandTimeout(600);
            await this.Database.ExecuteSqlCommandAsync(command);
        }
        #endregion

        public void Migrate()
        {
            this.Database.Migrate();
        }

        public void Seed()
        {
            var path = Directory.GetCurrentDirectory() + "/SeedScript.sql";
            var sepparator = new string[] { "GO" };
            StreamReader reader = new StreamReader(path);
            string command = reader.ReadToEnd();
            string[] commands = command.Split(sepparator, StringSplitOptions.None);
            reader.Close();
            for (int i = 0; i < commands.Length; i++)
            {
                if (!string.IsNullOrEmpty(commands[i].Trim()))
                {
                    this.Database.ExecuteSqlCommand(new RawSqlString(commands[i]));
                }
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.AddSNAMappings();
        }
    }

}