﻿using Microsoft.EntityFrameworkCore;
using SNA.Entities;

namespace SNA.DataAccess
{
	public static class SNAMapping
	{
		public static void AddSNAMappings(this ModelBuilder modelBuilder)
		{
			modelBuilder.Entity<InstaMedia>(entity =>
			{
				entity.HasKey(p => p.PK);
				entity.Property(p => p.PK).ValueGeneratedNever();
				entity.HasIndex(p => p.UserPK);
				entity.HasIndex(p => p.NextCrawlDate);
				entity.HasIndex(p => p.CrawledDate);
				entity.ToTable(nameof(InstaMedia));

				entity.HasOne(ro => ro.User)
					  .WithMany(r => r.Medias)
					  .HasForeignKey(ro => ro.UserPK)
					  .OnDelete(DeleteBehavior.Restrict);
			});

			modelBuilder.Entity<InstaMediaLiker>(entity =>
			{
				entity.HasKey(p => new { p.MediaPK,p.UserPK});
				entity.ToTable(nameof(InstaMediaLiker));

				entity.HasOne(ro => ro.User)
					  .WithMany(r => r.MediaLikers)
					  .HasForeignKey(ro => ro.UserPK)
					  .OnDelete(DeleteBehavior.Restrict);
				entity.HasOne(ro => ro.Media)
					  .WithMany(r => r.MediaLikers)
					  .HasForeignKey(ro => ro.MediaPK)
					  .OnDelete(DeleteBehavior.Restrict);
			});

			modelBuilder.Entity<InstaMediaComment>(entity =>
			{
				entity.HasKey(p => new { p.PK });
				entity.Property(p => p.PK).ValueGeneratedNever();
				entity.ToTable(nameof(InstaMediaComment));

				entity.HasOne(ro => ro.User)
					  .WithMany(r => r.MediaComments)
					  .HasForeignKey(ro => ro.UserPK)
					  .OnDelete(DeleteBehavior.Restrict);
				entity.HasOne(ro => ro.Media)
					  .WithMany(r => r.MediaComments)
					  .HasForeignKey(ro => ro.MediaPK)
					  .OnDelete(DeleteBehavior.Restrict);
			});

			modelBuilder.Entity<InstaUser>(entity =>
			{
				entity.HasKey(p => p.PK);
				entity.Property(p => p.PK).ValueGeneratedNever();
				entity.HasIndex(p => p.NextCrawlDate);
				entity.HasIndex(p => p.CrawledDate);
				entity.ToTable(nameof(InstaUser));
			});

			modelBuilder.Entity<InstaUserStarter>(entity =>
			{
				entity.HasKey(p => p.Username);
				entity.ToTable(nameof(InstaUserStarter));
			});

			modelBuilder.Entity<InstaUserHistory>(entity =>
			{
				entity.HasKey(p => new { p.HistoryDate,p.UserPK});
				entity.ToTable(nameof(InstaUserHistory));
			});

			modelBuilder.Entity<InstaUserFollow>(entity =>
			{
				entity.HasKey(p => new { p.SourcePK,p.TargetPK});
				entity.ToTable(nameof(InstaUserFollow));

				entity.HasOne(ro => ro.Target)
					  .WithMany(r => r.Followers)
					  .HasForeignKey(ro => ro.SourcePK)
					  .OnDelete(DeleteBehavior.Restrict);
				entity.HasOne(ro => ro.Source)
					  .WithMany(r => r.Followings)
					  .HasForeignKey(ro => ro.SourcePK)
					  .OnDelete(DeleteBehavior.Restrict);
			});
		}
	}
}