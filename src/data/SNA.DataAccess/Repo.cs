﻿using EFCore.BulkExtensions;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SNA.DataAccess
{
    public class Repo<T> : IRepo<T> where T:class
    {
        private readonly IDbContext _dbContext;
        public Repo(IDbContext dbContext)
        {
            _dbContext = dbContext;   
        }
        public void Insert(T entity)
        {
            _dbContext.Set<T>().Add(entity);

            _dbContext.SaveChanges();
        }

        public void Insert(IEnumerable<T> entities)
        {
            _dbContext.Set<T>().AddRange(entities);

            _dbContext.SaveChanges();
        }

        public async Task BulkInsertAsync(IList<T> entities)
        {
            await (_dbContext as DbContext).BulkInsertAsync(entities, new BulkConfig { PreserveInsertOrder = true, SetOutputIdentity = true, BatchSize = 4000 });
            await _dbContext.SaveChangesAsync();
        }

        public async Task BulkInsertSubEntityAsync(IList<T> entities)
        {
            await (_dbContext as DbContext).BulkInsertAsync(entities);
            await _dbContext.SaveChangesAsync();
        }

        public async Task InsertAsync(T entity)
        {
            _dbContext.Set<T>().Add(entity);

            await _dbContext.SaveChangesAsync();
        }

        public async Task InsertAsync(IEnumerable<T> entities)
        {
            _dbContext.Set<T>().AddRange(entities);

            await _dbContext.SaveChangesAsync();
        }

        public IQueryable<T> AsQueryable()
        {
            return _dbContext.Set<T>().AsQueryable();
        }

        public void Delete(T entity)
        {
            DeleteItem(entity);

            _dbContext.SaveChanges();
        }
        public async Task DeleteAsync(T entity)
        {
            DeleteItem(entity);

            await _dbContext.SaveChangesAsync();
        }

        private void DeleteItem(T entity)
        {
            //var entry = _dbContext.Entry<T>(entity);

            //if (entry.State == EntityState.Detached)
            //{
            //    entry.State = EntityState.Deleted;
            //}

            _dbContext.Set<T>().Remove(entity);
        }

        public void Delete(IEnumerable<T> entities)
        {
            foreach (var item in entities)
            {
                DeleteItem(item);
            }

            _dbContext.SaveChanges();
        }

        public async Task DeleteAsync(IEnumerable<T> entities)
        {
            foreach (var item in entities)
            {
                DeleteItem(item);
            }

            await _dbContext.SaveChangesAsync();
        }

        public void Update(T entity)
        {
            UpdateItem(entity);

            _dbContext.SaveChanges();
        }

        private void UpdateItem(T entity)
        {
            //var entry = _dbContext.Entry<T>(entity);

            //if (entry.State == EntityState.Detached)
            //{
            //    entry.State = EntityState.Modified;
            //}

            _dbContext.Set<T>().Update(entity);
        }

        public void Update(IEnumerable<T> entities)
        {
            foreach (var item in entities)
            {
                UpdateItem(item);
            }

            _dbContext.SaveChanges();
        }

        public async Task UpdateAsync(T entity)
        {
            UpdateItem(entity);

            await _dbContext.SaveChangesAsync();
        }

        public async Task UpdateAsync(IEnumerable<T> entities)
        {
            foreach (var item in entities)
            {
                UpdateItem(item);
            }

            await _dbContext.SaveChangesAsync();
        }

        public async Task BulkUpdateAsync(IList<T> entities)
        {
            await (_dbContext as DbContext).BulkUpdateAsync(entities);

            await _dbContext.SaveChangesAsync();
        }

        public async Task<int> SaveChangesAsync()
        {
            return await _dbContext.SaveChangesAsync();
        }
    }
}
