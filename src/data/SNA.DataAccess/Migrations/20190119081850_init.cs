﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SNA.DataAccess.Migrations
{
    public partial class init : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "InstaUser",
                columns: table => new
                {
                    PK = table.Column<long>(nullable: false),
                    CrawledDate = table.Column<DateTime>(nullable: true),
                    NextCrawlDate = table.Column<DateTime>(nullable: true),
                    FullName = table.Column<string>(nullable: true),
                    IsPrivate = table.Column<bool>(nullable: false),
                    IsVerified = table.Column<bool>(nullable: false),
                    ProfilePicUrl = table.Column<string>(nullable: true),
                    Username = table.Column<string>(nullable: true),
                    AddressStreet = table.Column<string>(nullable: true),
                    Biography = table.Column<string>(nullable: true),
                    Category = table.Column<string>(nullable: true),
                    CityId = table.Column<long>(nullable: false),
                    CityName = table.Column<string>(nullable: true),
                    ContactPhoneNumber = table.Column<string>(nullable: true),
                    DirectMessaging = table.Column<string>(nullable: true),
                    ExternalLynxUrl = table.Column<string>(nullable: true),
                    ExternalUrl = table.Column<string>(nullable: true),
                    FollowerCount = table.Column<long>(nullable: false),
                    FollowingCount = table.Column<long>(nullable: false),
                    FollowingTagCount = table.Column<long>(nullable: false),
                    FriendshipStatus = table.Column<string>(nullable: true),
                    GeoMediaCount = table.Column<long>(nullable: false),
                    MediaCount = table.Column<long>(nullable: false),
                    PageName = table.Column<string>(nullable: true),
                    PublicEmail = table.Column<string>(nullable: true),
                    PublicPhoneNumber = table.Column<string>(nullable: true),
                    UsertagsCount = table.Column<long>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_InstaUser", x => x.PK);
                });

            migrationBuilder.CreateTable(
                name: "InstaUserStarter",
                columns: table => new
                {
                    Username = table.Column<string>(nullable: false),
                    CrawledDate = table.Column<DateTime>(nullable: true),
                    NextCrawlDate = table.Column<DateTime>(nullable: true),
                    UserPK = table.Column<long>(nullable: true),
                    MediaCrawledDate = table.Column<DateTime>(nullable: true),
                    MediaNextCrawlDate = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_InstaUserStarter", x => x.Username);
                });

            migrationBuilder.CreateTable(
                name: "InstaMedia",
                columns: table => new
                {
                    PK = table.Column<string>(nullable: false),
                    CrawledDate = table.Column<DateTime>(nullable: true),
                    NextCrawlDate = table.Column<DateTime>(nullable: true),
                    UserPK = table.Column<long>(nullable: false),
                    Width = table.Column<int>(nullable: true),
                    Height = table.Column<int>(nullable: true),
                    LikesCount = table.Column<int>(nullable: false),
                    CommentsCount = table.Column<int>(nullable: false),
                    NextMediaId = table.Column<string>(nullable: true),
                    Title = table.Column<string>(nullable: true),
                    TakenAt = table.Column<DateTime>(nullable: false),
                    Caption = table.Column<string>(nullable: true),
                    CaptionIsEdited = table.Column<bool>(nullable: false),
                    ImageUrl = table.Column<string>(nullable: true),
                    LikerNextCrawlDate = table.Column<DateTime>(nullable: true),
                    CommentersNextCrawlDate = table.Column<DateTime>(nullable: true),
                    FollowLikeCount = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_InstaMedia", x => x.PK);
                    table.ForeignKey(
                        name: "FK_InstaMedia_InstaUser_UserPK",
                        column: x => x.UserPK,
                        principalTable: "InstaUser",
                        principalColumn: "PK",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "InstaUserFollow",
                columns: table => new
                {
                    SourcePK = table.Column<long>(nullable: false),
                    TargetPK = table.Column<long>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_InstaUserFollow", x => new { x.SourcePK, x.TargetPK });
                    table.ForeignKey(
                        name: "FK_InstaUserFollow_InstaUser_SourcePK",
                        column: x => x.SourcePK,
                        principalTable: "InstaUser",
                        principalColumn: "PK",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_InstaUserFollow_InstaUser_TargetPK",
                        column: x => x.TargetPK,
                        principalTable: "InstaUser",
                        principalColumn: "PK",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "InstaUserHistory",
                columns: table => new
                {
                    UserPK = table.Column<long>(nullable: false),
                    HistoryDate = table.Column<DateTime>(nullable: false),
                    FollowerCount = table.Column<long>(nullable: false),
                    FollowingCount = table.Column<long>(nullable: false),
                    FollowingTagCount = table.Column<long>(nullable: false),
                    MediaCount = table.Column<long>(nullable: false),
                    UsertagsCount = table.Column<long>(nullable: false),
                    InstaUserPK = table.Column<long>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_InstaUserHistory", x => new { x.HistoryDate, x.UserPK });
                    table.ForeignKey(
                        name: "FK_InstaUserHistory_InstaUser_InstaUserPK",
                        column: x => x.InstaUserPK,
                        principalTable: "InstaUser",
                        principalColumn: "PK",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "InstaMediaComment",
                columns: table => new
                {
                    PK = table.Column<long>(nullable: false),
                    MediaPK = table.Column<string>(nullable: true),
                    UserPK = table.Column<long>(nullable: false),
                    Text = table.Column<string>(nullable: true),
                    CreatedAt = table.Column<DateTime>(nullable: false),
                    LikesCount = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_InstaMediaComment", x => x.PK);
                    table.ForeignKey(
                        name: "FK_InstaMediaComment_InstaMedia_MediaPK",
                        column: x => x.MediaPK,
                        principalTable: "InstaMedia",
                        principalColumn: "PK",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_InstaMediaComment_InstaUser_UserPK",
                        column: x => x.UserPK,
                        principalTable: "InstaUser",
                        principalColumn: "PK",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "InstaMediaLiker",
                columns: table => new
                {
                    MediaPK = table.Column<string>(nullable: false),
                    UserPK = table.Column<long>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_InstaMediaLiker", x => new { x.MediaPK, x.UserPK });
                    table.ForeignKey(
                        name: "FK_InstaMediaLiker_InstaMedia_MediaPK",
                        column: x => x.MediaPK,
                        principalTable: "InstaMedia",
                        principalColumn: "PK",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_InstaMediaLiker_InstaUser_UserPK",
                        column: x => x.UserPK,
                        principalTable: "InstaUser",
                        principalColumn: "PK",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_InstaMedia_CrawledDate",
                table: "InstaMedia",
                column: "CrawledDate");

            migrationBuilder.CreateIndex(
                name: "IX_InstaMedia_NextCrawlDate",
                table: "InstaMedia",
                column: "NextCrawlDate");

            migrationBuilder.CreateIndex(
                name: "IX_InstaMedia_UserPK",
                table: "InstaMedia",
                column: "UserPK");

            migrationBuilder.CreateIndex(
                name: "IX_InstaMediaComment_MediaPK",
                table: "InstaMediaComment",
                column: "MediaPK");

            migrationBuilder.CreateIndex(
                name: "IX_InstaMediaComment_UserPK",
                table: "InstaMediaComment",
                column: "UserPK");

            migrationBuilder.CreateIndex(
                name: "IX_InstaMediaLiker_UserPK",
                table: "InstaMediaLiker",
                column: "UserPK");

            migrationBuilder.CreateIndex(
                name: "IX_InstaUser_CrawledDate",
                table: "InstaUser",
                column: "CrawledDate");

            migrationBuilder.CreateIndex(
                name: "IX_InstaUser_NextCrawlDate",
                table: "InstaUser",
                column: "NextCrawlDate");

            migrationBuilder.CreateIndex(
                name: "IX_InstaUserFollow_TargetPK",
                table: "InstaUserFollow",
                column: "TargetPK");

            migrationBuilder.CreateIndex(
                name: "IX_InstaUserHistory_InstaUserPK",
                table: "InstaUserHistory",
                column: "InstaUserPK");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "InstaMediaComment");

            migrationBuilder.DropTable(
                name: "InstaMediaLiker");

            migrationBuilder.DropTable(
                name: "InstaUserFollow");

            migrationBuilder.DropTable(
                name: "InstaUserHistory");

            migrationBuilder.DropTable(
                name: "InstaUserStarter");

            migrationBuilder.DropTable(
                name: "InstaMedia");

            migrationBuilder.DropTable(
                name: "InstaUser");
        }
    }
}
