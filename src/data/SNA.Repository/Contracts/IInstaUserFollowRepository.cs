﻿using SNA.Entities;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace SNA.Repository.Contracts
{
	public interface IInstaUserFollowRepository
	{
		Task<List<InstaUserFollow>> ReadAsync(Expression<Func<InstaUserFollow, bool>> condition);
		Task CreateAsync(InstaUserFollow model);
		Task CreateAsync(List<InstaUserFollow> model);
	}
}
