﻿using SNA.Entities;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace SNA.Repository.Contracts
{
	public interface IInstaMediaCommentRepository
	{
		Task<List<InstaMediaComment>> ReadAsync(Expression<Func<InstaMediaComment, bool>> condition, int? Take = null);
		Task CreateAsync(InstaMediaComment model);
		Task CreateAsync(List<InstaMediaComment> model);
	}
}
