﻿using SNA.Entities;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace SNA.Repository.Contracts
{
	public interface IInstaUserStarterRepository
	{
		Task<List<InstaUserStarter>> ReadAsync(Expression<Func<InstaUserStarter, bool>> condition, int? Take = null);
		Task UpdateAsync(InstaUserStarter model);
	}
}
