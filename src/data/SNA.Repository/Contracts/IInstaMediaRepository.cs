﻿using SNA.Entities;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace SNA.Repository.Contracts
{
	public interface IInstaMediaRepository
	{
		Task<List<InstaMedia>> ReadAsync(Expression<Func<InstaMedia, bool>> condition, int? Take = null);
		Task<List<string>> ReadAllAsync(Expression<Func<InstaMedia, bool>> condition);
		Task<int> CountAsync(Expression<Func<InstaMedia, bool>> condition);
		Task CreateAsync(InstaMedia model);
		Task CreateAsync(List<InstaMedia> model);
		Task UpdateAsync(InstaMedia model);
		Task UpdateAsync(List<InstaMedia> list);
	}
}
