﻿using SNA.Entities;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace SNA.Repository.Contracts
{
	public interface IInstaUserRepository
	{
		Task<List<InstaUser>> ReadAsync(
			Expression<Func<InstaUser, bool>> condition,
			Expression<Func<InstaUser, InstaUser>> selector,
			int? Take = null);
		Task<List<long>> ReadAllAsync();
		Task CreateAsync(InstaUser model);
		Task CreateAsync(List<InstaUser> model);
		Task UpdateAsync(InstaUser model);
		Task UpdateAsync(List<InstaUser> list);
	}
}
