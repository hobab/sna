﻿using SNA.Entities;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace SNA.Repository.Contracts
{
	public interface IInstaMediaLikerRepository
	{
		Task<List<InstaMediaLiker>> ReadAsync(Expression<Func<InstaMediaLiker, bool>> condition, int? Take = null);
		Task CreateAsync(InstaMediaLiker model);
		Task CreateAsync(List<InstaMediaLiker> model);
	}
}
