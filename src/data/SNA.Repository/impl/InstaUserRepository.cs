﻿using Microsoft.EntityFrameworkCore;
using SNA.DataAccess;
using SNA.Entities;
using SNA.Repository.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace SNA.Repository.Impl
{
	public class InstaUserRepository : IInstaUserRepository
	{
		private IUnitOfWork _uow;
		public InstaUserRepository(IUnitOfWork uow)
		{
			_uow = uow;
		}

		public async Task<List<InstaUser>> ReadAsync(
			Expression<Func<InstaUser, bool>> condition,
			Expression<Func<InstaUser, InstaUser>> selector,
			int? Take = null) 
		{
			var result = _uow.Repository<InstaUser>().AsQueryable().Where(condition).Select(selector);
			if (Take == null)
			{
				return await result.ToListAsync();
			}
			else
			{
				return await result.Take(Take.Value).ToListAsync();
			}
		}

		public async Task<List<long>> ReadAllAsync()
		{
			var result = await _uow.Repository<InstaUser>().AsQueryable().Select(c=>c.PK).ToListAsync();
			return result;
		}

		public async Task CreateAsync(InstaUser model)
		{
			await _uow.CommitAsync(async () =>
			{
				await _uow.Repository<InstaUser>().InsertAsync(model);
			});
		}

		public async Task CreateAsync(List<InstaUser> list)
		{
			await _uow.CommitAsync(async () =>
			{
				await _uow.Repository<InstaUser>().InsertAsync(list);
				//await _uow.Repository<InstaUser>().BulkInsertSubEntityAsync(list);
			});
		}

		public async Task UpdateAsync(InstaUser model)
		{
			await _uow.CommitAsync(async () =>
			{
				await _uow.Repository<InstaUser>().UpdateAsync(model);
			});
		}

		public async Task UpdateAsync(List<InstaUser> list)
		{
			await _uow.CommitAsync(async () =>
			{
				await _uow.Repository<InstaUser>().UpdateAsync(list);
			});
		}
	}
}
