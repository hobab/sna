﻿using Microsoft.EntityFrameworkCore;
using SNA.DataAccess;
using SNA.Entities;
using SNA.Repository.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace SNA.Repository.Impl
{
	public class InstaUserFollowRepository : IInstaUserFollowRepository
	{
		private IUnitOfWork _uow;
		public InstaUserFollowRepository(IUnitOfWork uow)
		{
			_uow = uow;
		}

		public async Task<List<InstaUserFollow>> ReadAsync(Expression<Func<InstaUserFollow, bool>> condition)
		{
			var result = await _uow.Repository<InstaUserFollow>()
									.AsQueryable()
									.Where(condition)
									.AsNoTracking()
									.ToListAsync();
			return result;
		}

		public async Task CreateAsync(InstaUserFollow model)
		{
			await _uow.CommitAsync(async () =>
			{
				await _uow.Repository<InstaUserFollow>().InsertAsync(model);
			});
		}

		public async Task CreateAsync(List<InstaUserFollow> list)
		{
			await _uow.CommitAsync(async () =>
			{
				await _uow.Repository<InstaUserFollow>().InsertAsync(list);
				//await _uow.Repository<InstaUserFollow>().BulkInsertSubEntityAsync(list);
			});
		}
	}
}
