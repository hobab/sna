﻿using Microsoft.EntityFrameworkCore;
using SNA.DataAccess;
using SNA.Entities;
using SNA.Repository.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace SNA.Repository.Impl
{
	public class InstaMediaCommentRepository : IInstaMediaCommentRepository
	{
		private readonly IUnitOfWork _uow;
		public InstaMediaCommentRepository(IUnitOfWork uow)
		{
			_uow = uow;
		}

		public async Task CreateAsync(InstaMediaComment model)
		{
			await _uow.CommitAsync(async () =>
			{
				await _uow.Repository<InstaMediaComment>().InsertAsync(model);
			});
		}

		public async Task CreateAsync(List<InstaMediaComment> model)
		{
			await _uow.CommitAsync(async () =>
			{
				await _uow.Repository<InstaMediaComment>().InsertAsync(model);
			});
		}

		public async Task<List<InstaMediaComment>> ReadAsync(Expression<Func<InstaMediaComment, bool>> condition, int? Take = null)
		{
			var result = _uow.Repository<InstaMediaComment>().AsQueryable().Where(condition);
			if (Take == null)
			{
				return await result.ToListAsync();
			}
			else
			{
				return await result.Take(Take.Value).ToListAsync();
			}
		}
	}
}
