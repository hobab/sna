﻿using Microsoft.EntityFrameworkCore;
using SNA.DataAccess;
using SNA.Entities;
using SNA.Repository.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace SNA.Repository.Impl
{
	public class InstaMediaRepository : IInstaMediaRepository
	{
		private readonly IUnitOfWork _uow;
		public InstaMediaRepository(IUnitOfWork uow)
		{
			_uow = uow;
		}

		public async Task CreateAsync(InstaMedia model)
		{
			await _uow.CommitAsync(async () =>
			{
				await _uow.Repository<InstaMedia>().InsertAsync(model);
			});
		}

		public async Task CreateAsync(List<InstaMedia> model)
		{
			await _uow.CommitAsync(async () =>
			{
				await _uow.Repository<InstaMedia>().InsertAsync(model);
			});
		}

		public async Task<List<InstaMedia>> ReadAsync(Expression<Func<InstaMedia, bool>> condition, int? Take = null)
		{
			var result = _uow.Repository<InstaMedia>().AsQueryable().Where(condition);
							 //.Include(c => c.MediaLikers).Include(c => c.MediaComments);
			if (Take == null)
			{
				return await result.ToListAsync();
			}
			else
			{
				return await result.Take(Take.Value).ToListAsync();
			}
		}

		public async Task<List<string>> ReadAllAsync(Expression<Func<InstaMedia, bool>> condition)
		{
			var result = await _uow.Repository<InstaMedia>().AsQueryable().Where(condition).Select(c => c.PK).ToListAsync();
			return result;
		}

		public async Task<int> CountAsync(Expression<Func<InstaMedia, bool>> condition)
		{
			var result = await _uow.Repository<InstaMedia>().AsQueryable().CountAsync(condition);
			return result;
		}

		public async Task UpdateAsync(InstaMedia model)
		{
			await _uow.CommitAsync(async () =>
			{
				await _uow.Repository<InstaMedia>().UpdateAsync(model);
			});
		}

		public async Task UpdateAsync(List<InstaMedia> list)
		{
			await _uow.CommitAsync(async () =>
			{
				await _uow.Repository<InstaMedia>().UpdateAsync(list);
			});
		}
	}
}
