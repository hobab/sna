﻿using Microsoft.EntityFrameworkCore;
using SNA.DataAccess;
using SNA.Entities;
using SNA.Repository.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace SNA.Repository.Impl
{
	public class InstaUserStarterRepository : IInstaUserStarterRepository
	{
		private IUnitOfWork _uow;
		public InstaUserStarterRepository(IUnitOfWork uow)
		{
			_uow = uow;
		}

		public async Task<List<InstaUserStarter>> ReadAsync(Expression<Func<InstaUserStarter, bool>> condition, int? Take = null)
		{
			var result = _uow.Repository<InstaUserStarter>().AsQueryable().Where(condition);
			if (Take == null)
			{
				return await result.ToListAsync();
			}
			else
			{
				return await result.Take(Take.Value).ToListAsync();
			}
		}

		public async Task UpdateAsync(InstaUserStarter model)
		{
			await _uow.CommitAsync(async () =>
			{
				await _uow.Repository<InstaUserStarter>().UpdateAsync(model);
			});
		}
	}
}
