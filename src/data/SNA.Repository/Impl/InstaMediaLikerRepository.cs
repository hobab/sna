﻿using Microsoft.EntityFrameworkCore;
using SNA.DataAccess;
using SNA.Entities;
using SNA.Repository.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace SNA.Repository.Impl
{
	public class InstaMediaLikerRepository : IInstaMediaLikerRepository
	{
		private readonly IUnitOfWork _uow;
		public InstaMediaLikerRepository(IUnitOfWork uow)
		{
			_uow = uow;
		}

		public async Task CreateAsync(InstaMediaLiker model)
		{
			await _uow.CommitAsync(async () =>
			{
				await _uow.Repository<InstaMediaLiker>().InsertAsync(model);
			});
		}

		public async Task CreateAsync(List<InstaMediaLiker> model)
		{
			await _uow.CommitAsync(async () =>
			{
				await _uow.Repository<InstaMediaLiker>().InsertAsync(model);
			});
		}

		public async Task<List<InstaMediaLiker>> ReadAsync(Expression<Func<InstaMediaLiker, bool>> condition, int? Take = null)
		{
			var result = _uow.Repository<InstaMediaLiker>().AsQueryable().Where(condition);
			if (Take == null)
			{
				return await result.ToListAsync();
			}
			else
			{
				return await result.Take(Take.Value).ToListAsync();
			}
		}
	}
}
