﻿using InstagramApiSharp.API;
using InstagramApiSharp.API.Builder;
using InstagramApiSharp.Classes;
using InstagramApiSharp.Classes.SessionHandlers;
using InstagramApiSharp.Logger;
using System;
using System.Diagnostics;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace SNA.Instagram
{
	public class AuthInstagram
	{
		public IInstaApi ApiInstance { get; set; }

		public IInstaApi GetDefaultInstaApiInstance(UserSessionData user, bool UseProxy)
		{
			var proxy = new WebProxy()
			{
				Address = new Uri($"http://185.208.77.199:3128"), //i.e: http://1.2.3.4.5:8080
				BypassProxyOnLocal = false,
				UseDefaultCredentials = false,
				// *** These creds are given to the proxy server, not the web server ***
				Credentials = new NetworkCredential(
					userName: "proxyclient",
					password: "123456")
			};
			var httpClientHandler = new HttpClientHandler()
			{
				Proxy = proxy,
			};
			bool needServerAuthentication = false;
			if (needServerAuthentication)
			{
				httpClientHandler.PreAuthenticate = true;
				httpClientHandler.UseDefaultCredentials = false;

				// *** These creds are given to the web server, not the proxy server ***
				httpClientHandler.Credentials = new NetworkCredential(
					userName: "root",
					password: "P@ssw0rd++@rman");
			}

			if (UseProxy)
			{
				var apiInstance = InstaApiBuilder.CreateBuilder()
						.UseHttpClientHandler(httpClientHandler)
						.SetUser(user)
						.UseLogger(new DebugLogger(LogLevel.Exceptions))
						.SetRequestDelay(RequestDelay.FromSeconds(0, 1))
						.SetSessionHandler(new FileSessionHandler() { FilePath = $"state-{user.UserName}.bin" })
						.Build();
				return apiInstance;
			}
			else
			{
				var apiInstance = InstaApiBuilder.CreateBuilder()
						.SetUser(user)
						.UseLogger(new DebugLogger(LogLevel.Exceptions))
						.SetRequestDelay(RequestDelay.FromSeconds(0, 1))
						.SetSessionHandler(new FileSessionHandler() { FilePath = $"state-{user.UserName}.bin" })
						.Build();
				return apiInstance;
			}
		}

		public async Task Login(string username, string password, bool UseProxy)
		{
			ApiInstance = GetDefaultInstaApiInstance(new UserSessionData
			{
				UserName = username,
				Password = password
			}, UseProxy);
			ApiInstance?.SessionHandler?.Load();
			if (!ApiInstance.IsUserAuthenticated)
			{
				var logInResult = await ApiInstance.LoginAsync();
				Debug.WriteLine(logInResult.Value);
				if (logInResult.Succeeded)
				{
					if (ApiInstance == null)
						return;
					if (!ApiInstance.IsUserAuthenticated)
						return;
					ApiInstance.SessionHandler.Save();
				}
				else
				{
					if (logInResult.Value == InstaLoginResult.ChallengeRequired)
					{
						var challenge = await ApiInstance.GetChallengeRequireVerifyMethodAsync();
						if (challenge.Succeeded)
						{
							if (challenge.Value.StepData != null)
							{
								if (!string.IsNullOrEmpty(challenge.Value.StepData.PhoneNumber))
								{
									//challange with sms
								}
								if (!string.IsNullOrEmpty(challenge.Value.StepData.Email))
								{
									//challange with email
								}
							}
						}
					}
				}
			}
		}
	}
}
