﻿using InstagramApiSharp;
using InstagramApiSharp.Classes;
using InstagramApiSharp.Classes.Models;
using Newtonsoft.Json;
using System.Net;
using System.Threading.Tasks;

namespace SNA.Instagram
{
	public class CrawlerInstagram
	{
		private readonly AuthInstagram _auth;
		public CrawlerInstagram(AuthInstagram auth)
		{
			_auth = auth;
		}
		public async Task Login(string username, string password, bool UseProxy = false)
		{
			await _auth.Login(username, password, UseProxy);
		}

		public async Task<IResult<InstaUserInfo>> GetUserInfo(string username)
		{
			return await _auth.ApiInstance.UserProcessor.GetUserInfoByUsernameAsync(username);
		}

		public async Task<IResult<InstaUserInfo>> GetUserInfo(long PK)
		{
			return await _auth.ApiInstance.UserProcessor.GetUserInfoByIdAsync(PK);
		}

		public async Task<IResult<InstaLikersList>> GetMediaLikers(string PK)
		{
			return await _auth.ApiInstance.MediaProcessor.GetMediaLikersAsync(PK);
		}

		public async Task<IResult<InstaCommentList>> GetMediaComments(string PK, PaginationParameters paggination)
		{
			return await _auth.ApiInstance.CommentProcessor.GetMediaCommentsAsync(PK, paggination);
		}

		public async Task<IResult<InstaMediaList>> GetUserMedias(string username, PaginationParameters paggination)
		{
			paggination.PagesLoaded = 1;
			return await _auth.ApiInstance.UserProcessor.GetUserMediaAsync(username, paggination);
		}

		public async Task<IResult<InstaUserShortList>> GetUserFollowings(string username, PaginationParameters paggination)
		{
			return await _auth.ApiInstance.UserProcessor.GetUserFollowingAsync(username, paggination);
		}

		public async Task<IResult<InstaUserShortList>> GetUserFollowers(string username, PaginationParameters paggination)
		{
			return await _auth.ApiInstance.UserProcessor.GetUserFollowersAsync(username, paggination);
		}
	}
}
