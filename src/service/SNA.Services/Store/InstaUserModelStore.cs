﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Text;

namespace SNA.Services.Store
{
	public static class InstaUserModelStore
	{
		public static readonly List<InstaUserModel> Users = new List<InstaUserModel>();
		public static readonly ConcurrentQueue<InstaUserModel> UsersQueue = new ConcurrentQueue<InstaUserModel>();
		public static void Init()
		{
			Users.Add(new InstaUserModel
			{
				Username = "hamid.reza.526",
				Password = "P@ssw0rd++"
			});
			Users.Add(new InstaUserModel
			{
				Username = "hr.hobab",
				Password = "P@ssw0rd++"
			});
			Users.Add(new InstaUserModel
			{
				Username = "hr.hobab.526",
				Password = "P@ssw0rd++"
			});

			foreach (var user in Users)
			{
				UsersQueue.Enqueue(user);
				UsersQueue.Enqueue(user);
			}
		}
	}
}
