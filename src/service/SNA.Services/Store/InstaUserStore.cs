﻿using SNA.Entities;
using System.Collections.Concurrent;

namespace SNA.Services.Store
{
	public class InstaUserEnqueuedStore : ConcurrentBag<InstaUser>
	{
	}

	public class InstaUserProccessedStore : ConcurrentBag<InstaUser>
	{
	}

	public class InstaUserDbStore : ConcurrentBag<long>
	{
		public static bool UserDbStoreSynced = false;
	}
}
