﻿using AutoMapper;
using InstagramApiSharp;
using SNA.common.Extensions;
using SNA.Entities;
using SNA.Instagram;
using SNA.Repository;
using SNA.Repository.Contracts;
using SNA.Services.Store;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SNA.Services.Impl
{
	public class InstaUserService : IInstaUserService
	{
		private readonly IInstaUserRepository _instaUserRepository;
		private readonly IInstaUserFollowRepository _instaUserFollowRepository;
		private readonly IInstaUserStarterRepository _instaUserStarterRepository;
		private readonly CrawlerInstagram _crawlerInsta;
		private readonly InstaUserEnqueuedStore _userEnqueuedStore;
		private readonly InstaUserProccessedStore _userProccessedStore;
		private readonly InstaUserDbStore _userDbStore;
		private readonly IMapper _mapper;
		private static PaginationParameters paggination = PaginationParameters.MaxPagesToLoad(5);

		public InstaUserService(
			IInstaUserRepository instaUserRepository,
			IInstaUserFollowRepository instaUserFollowRepository,
			IInstaUserStarterRepository instaUserStarterRepository,
			CrawlerInstagram crawlerInsta,
			InstaUserEnqueuedStore userEnqueuedStore,
			InstaUserProccessedStore userProccessedStore,
			InstaUserDbStore userDbStore,
			IMapper mapper)
		{
			_instaUserRepository = instaUserRepository;
			_instaUserFollowRepository = instaUserFollowRepository;
			_instaUserStarterRepository = instaUserStarterRepository;
			_crawlerInsta = crawlerInsta;
			_mapper = mapper;
			_userEnqueuedStore = userEnqueuedStore;
			_userProccessedStore = userProccessedStore;
			_userDbStore = userDbStore;
		}

		public async Task InstaUserDbStoreFromDB()
		{
			try
			{
				if (!_userDbStore.Any())
				{
					await Task.Delay(TimeSpan.FromSeconds(5));
					var allInstaUserDBStore = await _instaUserRepository.ReadAllAsync();
					_userDbStore.AddRange(allInstaUserDBStore);
					InstaUserDbStore.UserDbStoreSynced = true;

					Console.ForegroundColor = ConsoleColor.DarkGreen;
					Console.WriteLine($"Feed UserStore: {_userDbStore.Count}");
				}
				await Task.Delay(TimeSpan.FromMinutes(1));
			}
			catch (Exception ex)
			{
				Console.ForegroundColor = ConsoleColor.DarkRed;
				Console.WriteLine(ex.StackTrace);
				return;
			}
		}

		public async Task InstaUserStoreFromDB()
		{
			try
			{
				if (!_userEnqueuedStore.Any())
				{
					var DateNow = DateTime.Now;
					var allInstaUserCrawled = await _instaUserRepository.ReadAsync(c => c.NextCrawlDate == null || c.NextCrawlDate < DateNow, c => c, 1000);
					_userEnqueuedStore.AddRange(allInstaUserCrawled);
				}
				await Task.Delay(TimeSpan.FromMinutes(1));
			}
			catch (Exception ex)
			{
				Console.ForegroundColor = ConsoleColor.DarkRed;
				Console.WriteLine(ex.StackTrace);
				return;
			}
		}

		public async Task InstaUserStoreToDB()
		{
			try
			{
				if (_userProccessedStore.Any())
				{
					int proccessedCount = _userProccessedStore.Count;

					Console.ForegroundColor = ConsoleColor.DarkGreen;
					Console.Write($"userInfo crawled: {proccessedCount} - ");
					List<InstaUser> UserUpdateList = new List<InstaUser>();
					for (int i = 0; i < proccessedCount; i++)
					{
						_userProccessedStore.TryTake(out InstaUser user);
						UserUpdateList.Add(user);
					}
					await _instaUserRepository.UpdateAsync(UserUpdateList);

					Console.ForegroundColor = ConsoleColor.DarkGreen;
					Console.WriteLine($"updated: {proccessedCount}");
				}
				await Task.Delay(TimeSpan.FromMinutes(1));
			}
			catch (Exception ex)
			{
				Console.ForegroundColor = ConsoleColor.DarkRed;
				Console.WriteLine(ex.StackTrace);
				return;
			}
		}

		public async Task CrawlUserInfoAsync(bool UseProxy = false, int UserStoreNum = 0)
		{
			try
			{
				if (_userEnqueuedStore.Any())
				{
					InstaUserModel _userModel = InstaUserModelStore.Users[UserStoreNum];
					await _crawlerInsta.Login(_userModel.Username, _userModel.Password, UseProxy);

					Console.ForegroundColor = ConsoleColor.DarkGreen;
					Console.WriteLine($"CrawlInstaUserInfo >> User as Crawler: {_userModel.Username}");
					while (_userEnqueuedStore.TryTake(out InstaUser u))
					{
						//var _user = _mapper.Map<InstaUser>((await _crawlerInsta.GetUserInfo(u.Username)).Value);
						var _user = (await _crawlerInsta.GetUserInfo(u.PK)).Value;
						if (_user != null)
						{
							u.CrawledDate = DateTime.Now;
							u.NextCrawlDate = DateTime.Now.AddDays(20);
							u.AddressStreet = _user.AddressStreet;
							u.Biography = _user.Biography;
							u.Category = _user.Category;
							u.CityId = _user.CityId;
							u.CityName = _user.CityName;
							u.ContactPhoneNumber = _user.ContactPhoneNumber;
							u.DirectMessaging = _user.DirectMessaging;
							u.ExternalLynxUrl = _user.ExternalLynxUrl;
							u.ExternalUrl = _user.ExternalUrl;
							u.FollowerCount = _user.FollowerCount;
							u.FollowingCount = _user.FollowingCount;
							u.GeoMediaCount = _user.GeoMediaCount;
							u.MediaCount = _user.MediaCount;
							u.UsertagsCount = _user.UsertagsCount;
							u.FollowingTagCount = _user.FollowingTagCount;
							u.PageName = _user.PageName;
							u.PublicEmail = _user.PublicEmail;
							u.PublicPhoneNumber = _user.PublicPhoneNumber;
							_userProccessedStore.Add(u);
						}
						else
						{
							await Task.Delay(TimeSpan.FromSeconds(5));
						}
						await Task.Delay(TimeSpan.FromSeconds(1));
					}
					await Task.Delay(TimeSpan.FromMinutes(1));
				}
				else
				{
					await Task.Delay(TimeSpan.FromMinutes(1));
				}
			}
			catch (Exception ex)
			{
				Console.ForegroundColor = ConsoleColor.DarkRed;
				Console.WriteLine(ex.StackTrace);
				await Task.Delay(TimeSpan.FromSeconds(2));
				return;
			}
		}

		public async Task CrawlFollowersAsync()
		{
			try
			{
				var StarterUser = await _instaUserStarterRepository.ReadAsync(c => c.NextCrawlDate == null || c.NextCrawlDate < DateTime.Now);
				if (!StarterUser.Any() || !InstaUserDbStore.UserDbStoreSynced)
				{
					await Task.Delay(TimeSpan.FromSeconds(10));
					return;
				}
				InstaUserModel _userModel = InstaUserModelStore.Users[0];
				await _crawlerInsta.Login(_userModel.Username, _userModel.Password);

				Console.ForegroundColor = ConsoleColor.DarkGreen;
				Console.WriteLine($"CrawlFollower >> User as Crawler: {_userModel.Username}");

				string StarterUsername = StarterUser.FirstOrDefault().Username;
				var user = (await _crawlerInsta.GetUserInfo(StarterUsername)).Value;

				var allInstaUserFollowerCrawled = await _instaUserFollowRepository.ReadAsync(c => c.SourcePK == user.Pk);
				var allInstaUserFollowerPKCrawled = allInstaUserFollowerCrawled.Select(c => c.TargetPK).ToList();
				if (!_userDbStore.Contains(user.Pk))
				{
					await _instaUserRepository.CreateAsync(new InstaUser
					{
						PK = user.Pk,
						ProfilePicUrl = user.ProfilePicUrl,
						Username = user.Username,
						FullName = user.FullName,
						IsPrivate = user.IsPrivate,
						IsVerified = user.IsVerified
					});
					_userDbStore.Add(user.Pk);
				}

				int crawledCount = 0;
				while (crawledCount < user.FollowerCount)
				{
					var followers = await _crawlerInsta.GetUserFollowers(StarterUsername, paggination);
					crawledCount += followers.Value.Count;

					Console.ForegroundColor = ConsoleColor.DarkGreen;
					Console.Write($"user crawled: {crawledCount} - ");

					var f = followers.Value.Where(c => !_userDbStore.Contains(c.Pk)).ToList();
					var newFollowers = f.Select(c => new InstaUser
					{
						PK = c.Pk,
						ProfilePicUrl = c.ProfilePicUrl,
						Username = c.UserName,
						FullName = c.FullName,
						IsPrivate = c.IsPrivate,
						IsVerified = c.IsVerified
					}).ToList();
					if (newFollowers.Any())
					{
						await _instaUserRepository.CreateAsync(newFollowers);
						_userDbStore.AddRange(newFollowers.Select(c => c.PK).ToList());

						Console.ForegroundColor = ConsoleColor.DarkGreen;
						Console.Write($"user saved: {newFollowers.Count} - ");
					}

					var newFollowersLink = followers.Value.Where(c => !allInstaUserFollowerPKCrawled.Contains(c.Pk)).ToList();
					if (newFollowersLink.Any())
					{
						await _instaUserFollowRepository.CreateAsync(newFollowersLink.Select(c => new InstaUserFollow
						{
							SourcePK = user.Pk,
							TargetPK = c.Pk
						}).ToList());
						Console.ForegroundColor = ConsoleColor.DarkGreen;
						Console.WriteLine($"follower saved: {newFollowersLink.Count}");
					}

					_userDbStore.AddRange(newFollowers.Select(c => c.PK).ToList());
					allInstaUserFollowerPKCrawled.AddRange(newFollowersLink.Select(c => c.Pk).ToList());

					await Task.Delay(TimeSpan.FromSeconds(1));
				}
				//InstaUserModelStore.UsersQueue.Enqueue(_userModel);
				await Task.Delay(TimeSpan.FromSeconds(2));
			}
			catch (Exception ex)
			{
				Console.ForegroundColor = ConsoleColor.DarkRed;
				Console.WriteLine(ex.StackTrace);
				return;
			}
		}
	}
}