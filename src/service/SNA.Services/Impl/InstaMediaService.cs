﻿using AutoMapper;
using InstagramApiSharp;
using SNA.common.Extensions;
using SNA.Entities;
using SNA.Instagram;
using SNA.Repository.Contracts;
using SNA.Services.Store;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SNA.Services.Impl
{
	public class InstaMediaService : IInstaMediaService
	{
		private readonly IInstaMediaRepository _instaMediaRepository;
		private readonly IInstaMediaCommentRepository _instaMediaCommentRepository;
		private readonly IInstaUserStarterRepository _instaUserStarterRepository;
		private readonly IInstaUserRepository _instaUserRepository;
		private readonly CrawlerInstagram _crawlerInsta;
		private readonly InstaMediaEnqueuedStore _mediaEnqueuedStore;
		private readonly InstaMediaProccessedStore _mediaProccessedStore;
		private readonly InstaUserDbStore _userDbStore;
		private readonly IMapper _mapper;

		public InstaMediaService(
			IInstaMediaRepository instaMediaRepository,
			IInstaMediaCommentRepository instaMediaCommentRepository,
			IInstaUserStarterRepository instaUserStarterRepository,
			IInstaUserRepository instaUserRepository,
			CrawlerInstagram crawlerInsta,
			InstaMediaEnqueuedStore mediaEnqueuedStore,
			InstaMediaProccessedStore mediaProccessedStore,
			InstaUserDbStore userDbStore,
			IMapper mapper)
		{
			_instaMediaRepository = instaMediaRepository;
			_instaMediaCommentRepository = instaMediaCommentRepository;
			_instaUserStarterRepository = instaUserStarterRepository;
			_instaUserRepository = instaUserRepository;
			_crawlerInsta = crawlerInsta;
			_mediaEnqueuedStore = mediaEnqueuedStore;
			_mediaProccessedStore = mediaProccessedStore;
			_userDbStore = userDbStore;
			_mapper = mapper;
		}

		public async Task InstaMediaStoreFromDB()
		{
			try
			{
				if (!_mediaEnqueuedStore.Any())
				{
					var DateNow = DateTime.Now;
					var allInstaUserCrawled = await _instaMediaRepository.ReadAsync(c => c.NextCrawlDate < DateNow);
					_mediaEnqueuedStore.AddRange(allInstaUserCrawled);
				}
				await Task.Delay(TimeSpan.FromMinutes(2));
			}
			catch (Exception ex)
			{
				Console.ForegroundColor = ConsoleColor.DarkRed;
				Console.WriteLine(ex.StackTrace);
				await Task.Delay(TimeSpan.FromSeconds(10));
				return;
			}
		}

		public async Task InstaMediaStoreToDB()
		{
			try
			{
				if (_mediaProccessedStore.Any())
				{
					int proccessedCount = _mediaProccessedStore.Count;

					Console.ForegroundColor = ConsoleColor.DarkGreen;
					Console.Write($"Media crawled: {proccessedCount} - ");
					List<InstaMedia> MediaUpdateList = new List<InstaMedia>();
					for (int i = 0; i < proccessedCount; i++)
					{
						_mediaProccessedStore.TryTake(out InstaMedia user);
						MediaUpdateList.Add(user);
					}
					await _instaMediaRepository.UpdateAsync(MediaUpdateList);

					Console.ForegroundColor = ConsoleColor.DarkGreen;
					Console.WriteLine($"Media updated: {proccessedCount}");
				}
				await Task.Delay(TimeSpan.FromMinutes(1));
			}
			catch (Exception ex)
			{
				Console.ForegroundColor = ConsoleColor.DarkRed;
				Console.WriteLine(ex.StackTrace);
				await Task.Delay(TimeSpan.FromSeconds(10));
				return;
			}
		}

		public async Task CrawlMediaAsync()
		{
			try
			{
				var StarterUser = await _instaUserStarterRepository.ReadAsync(c => c.MediaNextCrawlDate == null || c.MediaNextCrawlDate < DateTime.Now);
				if (!StarterUser.Any() || !InstaUserDbStore.UserDbStoreSynced)
				{
					await Task.Delay(TimeSpan.FromMinutes(1));
					return;
				}
				InstaUserModel _userModel = InstaUserModelStore.Users[1];
				await _crawlerInsta.Login(_userModel.Username, _userModel.Password, true);

				Console.ForegroundColor = ConsoleColor.DarkGreen;
				Console.WriteLine($"CrawleMedia >> User as Crawler: {_userModel.Username}");

				string StarterUsername = StarterUser.FirstOrDefault().Username;
				var user = (await _crawlerInsta.GetUserInfo(StarterUsername)).Value;

				var allInstaMediaPKCrawled = await _instaMediaRepository.ReadAllAsync(c => c.UserPK == user.Pk);
				if (allInstaMediaPKCrawled.Count == user.MediaCount)
				{
					await Task.Delay(TimeSpan.FromMinutes(1));
					return;
				}

				int crawledCount = 0;
				var paggination = PaginationParameters.MaxPagesToLoad(5);
				while (crawledCount < user.MediaCount)
				{
					if (await _instaMediaRepository.CountAsync(c => c.UserPK == user.Pk) == user.MediaCount)
					{
						break;
					}

					var medias = await _crawlerInsta.GetUserMedias(StarterUsername, paggination);
					crawledCount += medias.Value.Count;
					Console.ForegroundColor = ConsoleColor.DarkGreen;
					Console.Write($"media crawled: {crawledCount} - ");

					var _newMedias = medias.Value.Where(c => !allInstaMediaPKCrawled.Contains(c.Pk)).ToList();
					if (_newMedias.Any())
					{
						var newMedias = new List<InstaMedia>();
						foreach (var c in _newMedias)
						{
							var media = new InstaMedia
							{
								PK = c.Pk,
								Caption = c.Caption?.Text,
								CommentsCount = Convert.ToInt32(c.CommentsCount),
								LikesCount = c.LikesCount,
								Height = c.Images?.FirstOrDefault()?.Height,
								Width = c.Images?.FirstOrDefault()?.Width,
								ImageUrl = c.Images?.FirstOrDefault()?.Uri,
								TakenAt = c.TakenAt,
								UserPK = user.Pk,
								CrawledDate = DateTime.Now,
								NextCrawlDate = DateTime.Now.AddDays(1)
							};
							newMedias.Add(media);
						}
						await _instaMediaRepository.CreateAsync(newMedias);
						Console.ForegroundColor = ConsoleColor.DarkGreen;
						Console.WriteLine($"media saved: {newMedias.Count}");
					}

					await Task.Delay(TimeSpan.FromSeconds(1));
				}
				StarterUser.FirstOrDefault().MediaCrawledDate = DateTime.Now;
				StarterUser.FirstOrDefault().MediaNextCrawlDate = DateTime.Now.AddDays(7);
				await _instaUserStarterRepository.UpdateAsync(StarterUser.FirstOrDefault());
			}
			catch (Exception ex)
			{
				Console.ForegroundColor = ConsoleColor.DarkRed;
				Console.WriteLine(ex.StackTrace);
				await Task.Delay(TimeSpan.FromSeconds(10));
				return;
			}
		}

		public async Task CrawlMediaLikersAsync()
		{
			try
			{
				if (!InstaUserDbStore.UserDbStoreSynced)
				{
					await Task.Delay(TimeSpan.FromSeconds(10));
					return;
				}
				InstaUserModel _userModel = InstaUserModelStore.Users[1];
				await _crawlerInsta.Login(_userModel.Username, _userModel.Password, true);
				Console.ForegroundColor = ConsoleColor.DarkGreen;
				Console.WriteLine($"CrawlMediaLikers >> User as Crawler: {_userModel.Username}");

				var allInstaMediaCrawled = await _instaMediaRepository.ReadAsync(c => c.LikerNextCrawlDate == null || c.LikerNextCrawlDate < DateTime.Now, 20);
				foreach (var media in allInstaMediaCrawled)
				{
					var likers = await _crawlerInsta.GetMediaLikers(media.PK);
					var newUserLikers = likers.Value.Where(d => !_userDbStore.Contains(d.Pk));
					if (newUserLikers.Any())
					{
						await _instaUserRepository.CreateAsync(newUserLikers.Select(d => new InstaUser
						{
							PK = d.Pk,
							ProfilePicUrl = d.ProfilePicUrl,
							Username = d.UserName,
							FullName = d.FullName,
							IsPrivate = d.IsPrivate,
							IsVerified = d.IsVerified
						}).ToList());
						Console.ForegroundColor = ConsoleColor.DarkGreen;
						Console.Write($"new User Likers Added: {newUserLikers.Count()} - ");
						_userDbStore.AddRange(newUserLikers.Select(c => c.Pk).ToList());
					}

					var existLikers = media.MediaLikers.Select(c => c.UserPK).ToList();
					var notExistLikers = likers.Value.Where(c => !existLikers.Contains(c.Pk));
					foreach (var liker in notExistLikers)
					{
						media.MediaLikers.Add(new InstaMediaLiker { UserPK = liker.Pk });
					}
					media.LikerNextCrawlDate = DateTime.Now.AddDays(7);
					await _instaMediaRepository.UpdateAsync(media);
				}
				await Task.Delay(TimeSpan.FromSeconds(5));
			}
			catch (Exception ex)
			{
				Console.ForegroundColor = ConsoleColor.DarkRed;
				Console.WriteLine(ex.StackTrace);
				await Task.Delay(TimeSpan.FromSeconds(10));
				return;
			}
		}

		public async Task CrawlMediaCommentsAsync()
		{
			try
			{
				if (!InstaUserDbStore.UserDbStoreSynced)
				{
					await Task.Delay(TimeSpan.FromSeconds(10));
					return;
				}
				InstaUserModel _userModel = InstaUserModelStore.Users[2];
				await _crawlerInsta.Login(_userModel.Username, _userModel.Password, true);
				Console.ForegroundColor = ConsoleColor.DarkGreen;
				Console.WriteLine($"CrawlMediaComments >> User as Crawler: {_userModel.Username}");

				var allInstaMediaCrawled = await _instaMediaRepository.ReadAsync(c => c.CommentersNextCrawlDate == null || c.CommentersNextCrawlDate < DateTime.Now, 20);
				foreach (var media in allInstaMediaCrawled)
				{
					var paggination = PaginationParameters.MaxPagesToLoad(5);
					List<InstagramApiSharp.Classes.Models.InstaUserShort> NewUsers = new List<InstagramApiSharp.Classes.Models.InstaUserShort>();
					var MediaComments = await _instaMediaCommentRepository.ReadAsync(c => c.MediaPK == media.PK);
					var NewMediaComments = new List<InstaMediaComment>();
					while (MediaComments.Count + NewMediaComments.Count < media.CommentsCount)
					{
						var comments = await _crawlerInsta.GetMediaComments(media.PK, paggination);
						var newUserCommenters = comments.Value.Comments.Where(d => !_userDbStore.Contains(d.User.Pk))
														.Select(c => c.User).Distinct().ToList();
						if (newUserCommenters.Any())
						{
							NewUsers.AddRange(newUserCommenters);
						}

						var existComments = MediaComments.Select(c => c.PK).ToList();
						var newComments = comments.Value.Comments.Where(c => !existComments.Contains(c.Pk));
						foreach (var comment in newComments)
						{
							NewMediaComments.Add(new InstaMediaComment
							{
								PK = comment.Pk,
								UserPK = comment.UserId,
								MediaPK = media.PK,
								CreatedAt = comment.CreatedAt,
								LikesCount = comment.LikesCount,
								Text = comment.Text
							});
						}
						await Task.Delay(TimeSpan.FromSeconds(1));
						if (paggination.NextMaxId == null)
						{
							break;
						}
					}
					var users = NewUsers.Distinct().Select(d => new InstaUser
					{
						PK = d.Pk,
						ProfilePicUrl = d.ProfilePicUrl,
						Username = d.UserName,
						FullName = d.FullName,
						IsPrivate = d.IsPrivate,
						IsVerified = d.IsVerified
					}).ToList();
					await _instaUserRepository.CreateAsync(users);
					_userDbStore.AddRange(users.Select(c => c.PK).ToList());

					await _instaMediaCommentRepository.CreateAsync(NewMediaComments);

					media.CommentersNextCrawlDate = DateTime.Now.AddDays(7);
					await _instaMediaRepository.UpdateAsync(media);

					Console.ForegroundColor = ConsoleColor.DarkGreen;
					Console.WriteLine($"MediaCommenters : {NewMediaComments.Count} - new User Commenters Added: {NewUsers.Count}");
				}
				await Task.Delay(TimeSpan.FromSeconds(5));
			}
			catch (Exception ex)
			{
				Console.ForegroundColor = ConsoleColor.DarkRed;
				Console.WriteLine(ex.StackTrace);
				await Task.Delay(TimeSpan.FromSeconds(10));
				return;
			}
		}
	}
}
