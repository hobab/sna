﻿using SNA.Services.Store;
using System.Threading.Tasks;

namespace SNA.Services
{
	public interface IInstaMediaService
	{
		Task InstaMediaStoreFromDB();
		Task InstaMediaStoreToDB();
		Task CrawlMediaAsync();
		Task CrawlMediaLikersAsync();
		Task CrawlMediaCommentsAsync();
	}
}
