﻿using SNA.Services.Store;
using System.Threading.Tasks;

namespace SNA.Services
{
	public interface IInstaUserService
	{
		Task InstaUserDbStoreFromDB();
		Task InstaUserStoreFromDB();
		Task InstaUserStoreToDB();
		Task CrawlFollowersAsync();
		Task CrawlUserInfoAsync(bool UseProxy = false, int UserStoreNum = 0);
	}
}
