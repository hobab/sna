﻿using InstagramApiSharp.Classes.Models;
using System;

namespace SNA.Services.Mapper
{
	public class AutoMapperProfile : AutoMapper.Profile
	{
		public AutoMapperProfile()
		{
			CreateMap<InstaUserShort, SNA.Entities.InstaUser>();

			CreateMap<InstaUserInfo, SNA.Entities.InstaUser>()
				.ForMember(dest => dest.CrawledDate, p => p.MapFrom(src => DateTime.Now))
				.ForMember(dest => dest.NextCrawlDate, p => p.MapFrom(src => DateTime.Now.AddDays(7)))
				.ForMember(dest => dest.Followers, p => p.Ignore())
				.ForMember(dest => dest.Followings, p => p.Ignore())
				.ForMember(dest => dest.Medias, p => p.Ignore());
		}				
	}
}
