﻿using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using SNA.common;
using SNA.DataAccess;
using System;
using System.IO;

namespace ReportGen.Api
{
	public class TemporaryDbContextFactory : IDesignTimeDbContextFactory<SNADbContext>
    {
        public SNADbContext CreateDbContext(string[] args)
        {
            var services = new ServiceCollection();
            services.AddOptions();
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();

            var basePath = Directory.GetCurrentDirectory();
            Console.WriteLine($"Using `{basePath}` as the ContentRootPath");

            var configuration = new ConfigurationBuilder()
                                .SetBasePath(basePath)
                                .AddJsonFile("appsettings.json", reloadOnChange: true, optional: false)
                                .AddEnvironmentVariables()
                                .Build();

            services.Configure<AppConfig>(options => configuration.GetSection("AppConfig").Bind(options));

            var siteSettings = services.BuildServiceProvider().GetRequiredService<IOptionsSnapshot<AppConfig>>();

            var optionsBuilder = new DbContextOptionsBuilder<SNADbContext>();

            Console.WriteLine(siteSettings.Value.SQLConnectionString);

            optionsBuilder.UseSqlServer(siteSettings.Value.SQLConnectionString);
            return new SNADbContext(optionsBuilder.Options);
        }
    }
}
