﻿using System;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;

namespace SNA.API
{
	public class Program
	{
		public static void Main(string[] args)
		{
			var configuration = new ConfigurationBuilder()
										.AddCommandLine(args)
											.Build();

			var hostUrl = configuration["hosturl"];

			if (string.IsNullOrEmpty(hostUrl))
			{
				hostUrl = "http://0.0.0.0:7070";
			}
			var host = new WebHostBuilder()
				.UseKestrel(options =>
				{
					options.Limits.MaxRequestBodySize = 20000000000;
					options.Limits.KeepAliveTimeout = TimeSpan.FromMinutes(10);
				})
				.UseUrls(hostUrl)
				.UseContentRoot(Directory.GetCurrentDirectory())
				.UseIISIntegration()
				.UseStartup<Startup>()
				.UseApplicationInsights()
				.Build();

			host.Run();
		}
	}
}
