﻿using System;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using SNA.DataAccess;
using SNA.common;
using Microsoft.Extensions.Options;
using SNA.IoCConfig;
using System.Security.Principal;
using System.Security.Claims;
using Newtonsoft.Json.Serialization;
using System.Reflection;
using AutoMapper;

namespace SNA.API
{
	public class Startup
	{
		public static IServiceProvider ServiceProvider { get; set; }
		public IConfigurationRoot Configuration { get; }
		public int IOptionsSnapShot { get; private set; }
		private IHostingEnvironment _currentEnvironment;
		private IHttpContextAccessor _contextAccessor;

		public Startup(IHostingEnvironment env, IHttpContextAccessor contextAccessor)
		{
			var builder = new ConfigurationBuilder()
				.SetBasePath(env.ContentRootPath)
				.AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
				.AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
				.AddEnvironmentVariables();
			Configuration = builder.Build();

			_currentEnvironment = env;
			_contextAccessor = contextAccessor;
		}

		// This method gets called by the runtime. Use this method to add services to the container.
		public void ConfigureServices(IServiceCollection services)
		{
			services.AddCors();

			services.AddSingleton<Microsoft.AspNetCore.Http.IHttpContextAccessor, HttpContextAccessor>();
			services.AddScoped<IPrincipal>(provider => provider.GetService<IHttpContextAccessor>()?.HttpContext?.User ?? ClaimsPrincipal.Current);

			services.AddMvc()
				.AddJsonOptions(options =>
				{
					options.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
				})
				.AddApplicationPart(Assembly.Load(new AssemblyName("SNA.Controller")));

			services.AddResponseCompression();

			services.Configure<AppConfig>(options => Configuration.GetSection("AppConfig").Bind(options));

			services.AddSingleton(p => ServiceProvider);

			services.AddDbContext<SNADbContext>(options =>
				options.UseSqlServer(
					ServiceProvider.GetService<IOptionsSnapshot<AppConfig>>().Value.SQLConnectionString,
					b => b.MigrationsAssembly("SNA.DataAccess")
				)
			);

			services.AddAutoMapper();
			services.AddDataServices();
			services.AddRepositoryServices();
			services.AddSNAServices();

			ServiceProvider = services.BuildServiceProvider();

			var uow = ServiceProvider.GetService<IDbContext>();
			uow.Migrate();
			uow.Seed();

			services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
		}

		// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
		public void Configure(IApplicationBuilder app, IHostingEnvironment env)
		{
			app.UseResponseCompression();
			app.UseCors(builder =>
			{
				builder.AllowAnyHeader();
				builder.AllowAnyMethod();
				builder.AllowAnyOrigin();
				builder.AllowCredentials();
				builder.WithExposedHeaders("WWW-Authenticate");
			});
			app.UseMvc();
		}
	}
}
