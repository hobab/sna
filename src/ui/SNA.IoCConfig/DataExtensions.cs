﻿using Microsoft.Extensions.DependencyInjection;
using SNA.DataAccess;

namespace SNA.IoCConfig
{
	public static class DataExtensions
	{
		public static IServiceCollection AddDataServices(this IServiceCollection services)
		{
			services.AddScoped<IDbContext, SNADbContext>();
			services.AddScoped<IUnitOfWork, SNADbContext>();
			services.AddScoped(typeof(IRepo<>), typeof(Repo<>));

			return services;
		}

		public static IServiceCollection AddTransiantDataServices(this IServiceCollection services)
		{
			services.AddTransient<IDbContext, SNADbContext>();
			services.AddTransient<IUnitOfWork, SNADbContext>();
			services.AddTransient(typeof(IRepo<>), typeof(Repo<>));

			return services;
		}
	}
}
