﻿using Microsoft.Extensions.DependencyInjection;
using SNA.Repository;
using SNA.Repository.Contracts;
using SNA.Repository.Impl;

namespace SNA.IoCConfig
{
	public static class RepositoryExtensions
	{
		public static IServiceCollection AddRepositoryServices(this IServiceCollection services)
		{
			services.AddScoped<IInstaMediaRepository, InstaMediaRepository>();
			services.AddScoped<IInstaMediaLikerRepository, InstaMediaLikerRepository>();
			services.AddScoped<IInstaMediaCommentRepository, InstaMediaCommentRepository>();
			services.AddScoped<IInstaUserRepository, InstaUserRepository>();
			services.AddScoped<IInstaUserFollowRepository, InstaUserFollowRepository>();
			services.AddScoped<IInstaUserStarterRepository, InstaUserStarterRepository>();

			return services;
		}

		public static IServiceCollection AddTransientRepositoryServices(this IServiceCollection services)
		{
			services.AddTransient<IInstaMediaRepository, InstaMediaRepository>();
			services.AddTransient<IInstaMediaLikerRepository, InstaMediaLikerRepository>();
			services.AddTransient<IInstaMediaCommentRepository, InstaMediaCommentRepository>();
			services.AddTransient<IInstaUserRepository, InstaUserRepository>();
			services.AddTransient<IInstaUserFollowRepository, InstaUserFollowRepository>();
			services.AddTransient<IInstaUserStarterRepository, InstaUserStarterRepository>();

			return services;
		}
	}
}
