﻿using Microsoft.Extensions.DependencyInjection;
using SNA.Instagram;
using SNA.Services;
using SNA.Services.Impl;
using SNA.Services.Store;

namespace SNA.IoCConfig
{
	public static class ServiceExtensions
	{
		public static IServiceCollection AddSNAServices(this IServiceCollection services)
		{
			services.AddScoped<AuthInstagram>();
			services.AddScoped<CrawlerInstagram>();

			services.AddScoped<IInstaUserService, InstaUserService>();
			services.AddScoped<IInstaMediaService, InstaMediaService>();

			return services;
		}

		public static IServiceCollection AddTransientSNAServices(this IServiceCollection services)
		{
			services.AddTransient<AuthInstagram>();
			services.AddTransient<CrawlerInstagram>();

			services.AddTransient<IInstaUserService, InstaUserService>();
			services.AddTransient<IInstaMediaService, InstaMediaService>();

			services.AddSingleton(typeof(InstaUserEnqueuedStore));
			services.AddSingleton(typeof(InstaUserProccessedStore));
			services.AddSingleton(typeof(InstaMediaEnqueuedStore));
			services.AddSingleton(typeof(InstaMediaProccessedStore));
			services.AddSingleton(typeof(InstaUserDbStore));

			return services;
		}
	}
}
